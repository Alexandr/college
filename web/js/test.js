$(document).ready(function() {

});

function openTest() {
    $.ajax({
        type: "GET",
        url: "/test/start",
        success: function(response) {
            $('.testContent').html(response);
            $('#collapseTest').collapse('show');
        }
    });
}

function voprosOtvet(numb) {
    let value = 0;
    $('.otvetVopros').each(function(index, element) {
        let radioElement = $(this).find('input[type="radio"]');
        if (radioElement.is(':checked')) {
            value = radioElement.val();
        }
    });

    $.ajax({
        type: "GET",
        url: "/test/otvet",
        data: { 'numb': numb, 'val': value },
        success: function(response) {
            $('.testContent').html(response);
            // $('#collapseTest').collapse('show');
        }
    });

}

function clearTest() {
    $.ajax({
        type: "GET",
        url: "/test/clear",
        success: function(response) {
            $.ajax({
                type: "GET",
                url: "/test/start",
                success: function(response) {
                    $('.testContent').html(response);
                    $('#collapseTest').collapse('show');
                }
            });
        }
    });
}