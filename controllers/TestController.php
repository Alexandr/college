<?php

namespace app\controllers;

use app\models\Test;
use Yii;
use yii\web\Controller;


class TestController extends Controller
{
    public function actionStart()
    {
        $this->layout = false;

        $session = Yii::$app->session;
        if ($session->isActive) $session->open();
        $test = $session->get('Test');
        if ($test == null) {
            $model = Test::find()->orderBy(['weight' => SORT_ASC])->one();
        } else {
            $last = array_key_last($test);
            // debug($last);
            $model = Test::findOne(['weight'=>$last]);
        }


        return $this->render('start', [
            'model' => $model,
            'test' => $test,
        ]);
    }

    public function actionOtvet()
    {
        $this->layout = false;

        $numb = Yii::$app->request->get('numb');
        $val = Yii::$app->request->get('val');
        $otvet = Test::findOne(['weight' => $numb - 1]);

        $session = Yii::$app->session;
        if ($session->isActive) $session->open();
        $test = $session->get('Test');
        if ($otvet->otvet == $val) {
            $test[$numb] = 1;
        } else {
            $test[$numb] = 0;
        }

        $session->set('Test', $test);


        return $this->render('otvet', [
            'otvet' => $otvet,
            'model' => $test[$numb],
        ]);
    }

    public function actionClear() {
        $session = Yii::$app->session;
        if ($session->isActive) $session->open();
        $session->remove('Test');
        return true;
    }
}
