<?php

namespace app\controllers;

use app\models\Article;
use app\models\Category;
use yii\data\Pagination;
use yii\web\Controller;

class ArticleController extends Controller
{
    public function actionView($id)
    {
        $model = Article::findOne($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
