<?php

namespace app\controllers;

use app\models\Article;
use app\models\Category;
use app\models\Gallery;
use yii\data\Pagination;
use yii\web\Controller;

class GalleryController extends Controller
{
    public function actionIndex()
    {
        $model = Gallery::find()->orderBy(['weight'=>SORT_ASC])->all();

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
