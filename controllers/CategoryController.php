<?php

namespace app\controllers;

use app\models\Article;
use app\models\Category;
use yii\data\Pagination;
use yii\web\Controller;

class CategoryController extends Controller
{
    public function actionView($id)
    {
        $model = Category::findOne($id);

        $query = Article::find()->orderBy(['date_create' => SORT_ASC])->where(['category_id' => $id]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $articles = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('view', [
            'model' => $model,
            'articles' => $articles,
            'pages' => $pages,
        ]);
    }
}
