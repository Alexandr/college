<?php

use yii\helpers\Html;
use yii\helpers\Json;

$data = Json::decode($model->data);

?>
<a class="float-left d-flex text-decoration-none text-success w-100 align-items-center mb-2" href="/web/<?= $data['file'] ?>" download="<?= $data['fileName'] ?>"><i class="far fa-file mr-3"></i>
    <h5 class="mb-0"><?= $data['title'] ?></h5>
</a>