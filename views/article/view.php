<?php

use app\models\Article;
use app\models\FrontText;
use yii\helpers\Url;

$lasArticle = Article::find()->orderBy(['date_create' => SORT_DESC])->where(['!=', 'id', $model->id])->limit(4)->all();


$this->title = $model->name;
?>
<header class="w-100 header-category d-flex justify-content-center align-items-center mb-5" style="background-image: url(/web/<?=str_replace("\\", "/", $model->getImage()->getPath()) ?>);">
    <div class="header-category-over"></div>
    <h1 class="text-center text-white position-relative"><?= $this->title ?></h1>
</header>
<div class="container">
    <div class="row">
        <div class="col-12 col-md-9">
            <?php if ($model->articleWidgets != null) : ?>
                <?php foreach ($model->articleWidgets as $key => $widget) : ?>
                    <div class="">
                        <?php if ($widget->type == 1) : ?>
                            <?= $this->render('widget/text', [
                                'model' => $widget,
                            ]) ?>
                        <?php elseif ($widget->type == 2) : ?>
                            <?= $this->render('widget/image', [
                                'model' => $widget,
                            ]) ?>
                        <?php elseif ($widget->type == 3) : ?>
                            <?= $this->render('widget/doc', [
                                'model' => $widget,
                            ]) ?>
                        <?php endif ?>
                    </div>
                <?php endforeach ?>
            <?php endif ?>
        </div>
        <div class="col-12 col-md-3">
            <?php if ($lasArticle != null) : ?>
                <h4 class="mb-4">Недавние статьи</h4>
                <?php foreach ($lasArticle as $key => $valueArt) : ?>
                    <a href="<?= Url::to(['/article/view', 'id' => $valueArt->id]) ?>" class="mb-4 float-left w-100">
                        <div class="d-flex">
                            <img src="/web/<?= str_replace("\\", "/", $valueArt->getImage()->getPath('600x600')) ?>" alt="" class="previewArt">
                            <div class="">
                                <h3 class="text-dark" style="font-size: 18px;"><?= $valueArt->name ?></h3>
                                <p class="text-dark" style="font-size: 12px;"><?= Yii::$app->formatter->asDate($valueArt->date_create, 'php:M d, Y') ?></p>
                            </div>
                        </div>
                    </a>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</div>