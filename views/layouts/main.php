<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\MenuTop;
use yii\helpers\Url;

$menuTop = MenuTop::find()->where(['parent_id' => 0])->orderBy(['weight' => SORT_ASC])->all();

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light shadow">
            <div class="container">
                <a class="navbar-brand" href="/"><img src="/web/images/logo.svg" alt="Сайт учителя английского языка" class=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav justify-content-end w-100">
                        <?php if ($menuTop != null) : ?>
                            <?php foreach ($menuTop as $key => $menuItem) : ?>
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?= Url::to([$menuItem->link]) ?>" <?= $menuItem->link_attribute ?>><?= $menuItem->name ?></a>
                                </li>
                            <?php endforeach ?>
                        <?php endif ?>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="w-100">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
    <section class="pt-5 pb-5"></section>
    <footer class="footer pt-5 pb-5">
        <div class="container">
            <p class="pull-left">Copyright ©2020 Мартынова А. А. |  Публикация материалов с сайта разрешена только при наличии активной ссылки на главную страницу</p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>