<?php

use app\models\FrontText;
use yii\helpers\Url;

$path = str_replace("\\", "/", $model->getImage()->getPath());
// debug($path);

$this->title = $model->name;
?>
<header class="w-100 header-category d-flex justify-content-center align-items-center mb-5" style="background-image: url(/web/<?= $path ?>);">
    <div class="header-category-over"></div>
    <h1 class="text-center text-white position-relative"><?= $this->title ?></h1>
</header>
<div class="container pt-5 pb-5">
    <?php if ($articles != null) : ?>
        <div class="row">
            <?php foreach ($articles as $key => $article) : ?>
                <div class="col-12 col-md-4">
                    <a href="<?= Url::to(['/article/view', 'id' => $article->id]) ?>" class="card">
                        <img src="/web/<?= str_replace("\\", "/", $article->getImage()->getPath('600x400')); ?>" alt="" class="img-fluid">
                        <div class="card-body">
                            <h3 class="text-dark" style="font-size: 21px;"><?= $article->name ?></h3>
                            <p class="text-dark"><?= $article->body ?></p>
                        </div>
                    </a>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>
</div>