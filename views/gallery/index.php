<?php

use app\models\FrontText;
use yii\helpers\Url;

$this->title = 'ФОТОГАЛЕРЕЯ';
?>
<header class="w-100 header-category d-flex justify-content-center align-items-center mb-5" style="background-image: url(/web/images/bggall.jpg);">
    <div class="header-category-over"></div>
    <h1 class="text-center text-white position-relative"><?= $this->title ?></h1>
</header>
<div class="container pt-5 pb-5">
    <?php if ($model != null) : ?>
        <div class="row">
            <?php foreach ($model as $key => $value) : ?>
                <div class="col-12 col-md-4 mb-4">
                    <a href="/web/<?= str_replace("\\", "/", $value->getImage()->getPath('600x400')) ?>" data-fancybox="gallery" data-caption="<?= $value->name ?>" class="card shadow">
                        <img src="/web/<?= str_replace("\\", "/", $value->getImage()->getPath('600x400')) ?>" alt="" class="img-fluid">
                    </a>
                </div>
            <?php endforeach ?>
        </div>
    <?php endif ?>
</div>