<?php

use app\models\FrontText;

$frontText = new FrontText();
$frontText->openModel();



$this->title = 'Сайт учителя английского языка Кузнецова Людмила Александровна';
?>
<header class="w-100 vh-100 header-font d-flex justify-content-center align-items-center mb-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-6">
                <h6 class="font-weight-bold" style="color: #D7CECE;">ПРЕПОДАВАТЕЛЬ ГАПОУ ПО <br>“Пензенский колледж архитектуры и строительства”</h6>
                <h1 class="text-white font-weight-bold">Кузнецова Людмила Александровна</h1>
            </div>
            <div class="col-12 col-md-6">
                <div class="photo-rect-wrap">
                    <img src="/web/images/uchitel.jpg" alt="" class="photo-rect">
                </div>
            </div>
        </div>
    </div>

</header>
<section>
    <div class="container pt-5 pb-5">
        <h1 class="text-center"><?= $frontText->name ?></h1>
        <h6 class="text-center mt-4 mb-5"><?= $frontText->subName ?></h6>
        <div class="row pt-5">
            <div class="col-12 col-md-7">
                <?= $frontText->text ?>
            </div>
            <div class="col-12 col-md-5">
                <img src="/web/<?= $frontText->imageFile->getPath() ?>" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</section>
<section class="bgover-light pt-5 pb-5">
    <div class="container">
        <h6 class="text-center"><i class="fas fa-star" style="color: #D7D5D5;"></i> <span>ОПРЕДЕЛИМ УРОВЕНЬ</span></h6>
        <h1 class="font-weight-bold text-center">узнайте свой уровень английского!</h1>
        <div class="d-flex w-100 justify-content-center pt-4">
            <button type="button" class="btn btn-secondary btn-lg" onclick="openTest()">Пройти тест</button>
        </div>
        <div class="collapse mt-4" id="collapseTest">
            <div class="card card-body testContent"></div>
        </div>
    </div>
</section>