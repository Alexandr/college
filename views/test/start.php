<?php

use yii\helpers\Json;

// debug($test);
if ($model != null) {
    $voproses = Json::decode($model->vopros);
} else {
    $ball = 0;
    foreach ($test as $key => $value) {
        $ball = $ball + $value;
    }
}

?>
<?php if ($model != null) : ?>
    <h2>Вопрос №<?= ($model->weight + 1) ?></h2>
    <hr>
    <h3><?= $model->name ?></h3>

    <div class="row align-items-end">
        <div class="col-12 col-md-8">
            <?php foreach ($voproses as $key => $vopros) : ?>
                <div class="d-flex align-items-center w-100 float-left mb-3 pl-5 otvetVopros">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleTests<?= $key + 1 ?>" value="<?= $key + 1 ?>" checked>
                    <label class="form-check-label" for="exampleTests<?= $key + 1 ?>">
                        <h4 class="mb-0"><?= $vopros ?></h4>
                    </label>
                </div>
            <?php endforeach ?>
        </div>
        <div class="col-12 col-md-4"><button type="button" class="btn btn-outline-dark" onclick="voprosOtvet(<?= ($model->weight + 1) ?>)">Ответить</button></div>
    </div>
<?php else : ?>
    <h2>Тест пройден</h2>
    <h4>Ваши балы: <?= $ball ?></h4>
    <p>Количество правильных ответов <b>до 5</b> - уровень владения Beginner,<br> <b>от 6 до 10</b> - уровень владения Elementary,<br> <b>от 11 - до 15</b> - уровень владения Pre-Intermediate,<br> <b>от 16 до 20</b> - уровень владения Intermediate,<br> <b>от 21 до 25</b> - уровень владения Upper-Intermediate,<br> <b>от 26 до 30</b> - уровень владения Advanced)</p>
    <div class="col-12 col-md-4"><button type="button" class="btn btn-outline-dark" onclick="clearTest()">Пройти заново</button></div>
<?php endif ?>