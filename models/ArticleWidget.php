<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "articleWidget".
 *
 * @property int $id
 * @property int $weight
 * @property int $articleId
 * @property int $type
 * @property string $data
 *
 * @property Article $article
 */
class ArticleWidget extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articleWidget';
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'alex290\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public $imageFile;
    
    public function rules()
    {
        return [
            [['weight', 'articleId', 'data'], 'required'],
            [['weight', 'articleId', 'type'], 'integer'],
            [['data'], 'safe'],
            [['articleId'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['articleId' => 'id']],
            [['imageFile'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'weight' => 'Weight',
            'articleId' => 'Article ID',
            'type' => '1-Текст, 2-Картинка',
            'data' => 'Data',
        ];
    }

    /**
     * Gets query for [[Article]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'articleId']);
    }

    public function upload()
    {
        if ($this->validate()) {
            $path = 'upload/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs($path);
            $this->attachImage($path);
            unlink($path);
            return true;
        } else {
            return false;
        }
    }
}
