<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\Json;

class ArticleText extends Model
{
    public $text;
    public $weight;
    public $model;

    public function rules()
    {
        return [
            [['text'], 'string'],
            [['weight'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'text' => 'Текст',
        ];
    }

    public function newModel($id)
    {
        $model = new ArticleWidget();
        $model->articleId = $id;
        $model->type = 1;
        $model->weight = ArticleWidget::find()->where(['articleId' => $id])->count();
        $this->weight = $model->weight;
        $this->model = $model;
    }

    public function openModel($id)
    {
        $model = ArticleWidget::findOne($id);
        if ($model == null) {
            $model = new ArticleWidget();
            $model->articleId = $id;
            $model->type = 1;
            $model->weight = ArticleWidget::find()->where(['articleId' => $id])->count();
        } else {
            $data = Json::decode($model->data);
            if (array_key_exists('text', $data)) {
                $this->text = $data['text'];
            }
        }
        $this->weight = $model->weight;
        $this->model = $model;
    }

    public function saveModel()
    {
        $model = $this->model;

        $model->data = Json::encode([
            'text' => $this->text,
        ]);
        $model->save();
    }
}
