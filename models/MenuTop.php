<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menuTop".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string|null $link
 * @property int $weight
 * @property string|null $link_attribute
 */
class MenuTop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu_top';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id'], 'required'],
            [['parent_id', 'weight'], 'integer'],
            [['name', 'link', 'link_attribute'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Название',
            'link' => 'Ссылка',
            'weight' => 'Weight',
            'link_attribute' => 'Атрибут ссылки',
        ];
    }
}
