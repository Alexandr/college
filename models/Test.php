<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "test".
 *
 * @property int $id
 * @property string $vopros
 * @property int $otvet
 * @property int $weight
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['otvet', 'weight'], 'required'],
            [['vopros'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['otvet', 'weight'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'vopros' => 'Вопросы',
            'otvet' => 'Ответ',
            'weight' => 'Вес',
        ];
    }
}
