<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\Json;

class Vopros extends Model
{
    public $text;

    public function rules()
    {
        return [
            [['text'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'text' => 'Текст',
        ];
    }

    
}
