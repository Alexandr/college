<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "articleComment".
 *
 * @property int $id
 * @property int $articleId
 * @property int $create
 * @property string $name
 * @property string $text
 *
 * @property Article $article
 */
class ArticleComment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['articleId', 'create', 'name', 'text'], 'required'],
            [['articleId', 'create'], 'integer'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 50],
            [['articleId'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['articleId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'articleId' => 'Article ID',
            'create' => 'Дата',
            'name' => 'Имя',
            'text' => 'Текст',
        ];
    }

    /**
     * Gets query for [[Article]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'articleId']);
    }
}
