<?php

namespace app\models;

use app\models\Setting;
use Yii;
use yii\base\Model;
use yii\helpers\Json;

class FrontText extends Model
{


    public $text;
    public $name;
    public $subName;
    public $image;
    public $imageFile;

    public function rules()
    {
        return [
            [['text', 'name', 'subName'], 'string'],
            [['image'], 'file', 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'subName' => 'Подзагаловок',
            'text' => 'Текст',
            'image' => 'Изображение',
        ];
    }

    public function openModel()
    {
        $model = Setting::findOne(['name' => 'frontText']);
        if ($model == null) {
            $model = new Setting();
            $model->name = 'frontText';
            $model->data = Json::encode([
                'text' => '',
                'name' => '',
                'subName' => '',
            ]);
            $model->save();
        } else {
            $data = Json::decode($model->data);
            if ($data['text']) {
                $this->text = $data['text'];
            }
            if (array_key_exists('name', $data)) {
                $this->name = $data['name'];
            }

            if (array_key_exists('subName', $data)) {
                $this->subName = $data['subName'];
            }
            
        }
        $this->imageFile = $model->getImage();
    }

    public function saveModel()
    {
        $model = Setting::findOne(['name' => 'frontText']);
        if ($model == null) {
            $model = new Setting();
            $model->name = 'frontText';
        }

        $model->data = Json::encode([
            'text' => $this->text,
            'name' => $this->name,
            'subName' => $this->subName,
        ]);
        $model->save();
    }

    public function upload()
    {
        if ($this->validate()) {
            $path = 'upload/images' . $this->image->baseName . '.' . $this->image->extension;

            $model = Setting::findOne(['name' => 'frontText']);
            if ($model == null) {
                $model = new Setting();
                $model->name = 'frontText';
                $model->data = Json::encode([
                    'text' => '',
                    'name' => '',
                    'subName' => '',
                ]);
                $model->save();
            }
            $model->removeImages();
            $this->image->saveAs($path);
            $model->attachImage($path);
            unlink($path);
            return true;
        } else {
            return false;
        }
    }
}
