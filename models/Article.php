<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $name
 * @property int $category_id
 * @property int $date_create
 * @property string|null $body
 * @property int|null $frontpage
 * @property string $alias
 * @property string|null $meta_description
 * @property int|null $count
 *
 * @property Category $category
 * @property ArticleComment[] $articleComments
 * @property ArticleWidget[] $articleWidgets
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'alex290\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public $imageFile;
    
    public function rules()
    {
        return [
            [['name', 'category_id', 'date_create'], 'required'],
            [['category_id', 'date_create', 'frontpage', 'count'], 'integer'],
            [['body'], 'string'],
            [['name', 'alias', 'meta_description'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['imageFile'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'category_id' => 'Категория',
            'date_create' => 'Дата создания',
            'body' => 'Анонс',
            'frontpage' => 'На главной',
            'alias' => 'URL',
            'meta_description' => 'Meta Description',
            'count' => 'Просмотры',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function upload()
    {
        if ($this->validate()) {
            $path = 'upload/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $this->imageFile->saveAs($path);
            $this->attachImage($path);
            unlink($path);
            return true;
        } else {
            return false;
        }
    }

    public function getArticleComments()
    {
        return $this->hasMany(ArticleComment::className(), ['articleId' => 'id'])->orderBy(['create'=>SORT_ASC]);
    }

    public function getArticleWidgets()
    {
        return $this->hasMany(ArticleWidget::className(), ['articleId' => 'id'])->orderBy(['weight'=>SORT_ASC]);
    }
}
