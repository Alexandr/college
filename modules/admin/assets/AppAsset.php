<?php


namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets/scr';
    public $css = [
        'jquery-ui/jquery-ui.min.css',
        'fileinput/css/fileinput.css',
        'air-datepicker/css/datepicker.min.css',
        'select2/css/select2.css',
        'fileinput/themes/explorer-fas/theme.css',
        'css/admin.css',
    ];
    public $js = [
        'jquery-ui/jquery-ui.min.js',
        'fileinput/js/fileinput.js',
        'air-datepicker/js/datepicker.min.js',
        'fileinput/js/locales/ru.js',
        'fileinput/themes/fas/theme.js',
        'fileinput/themes/explorer-fas/theme.js',
        'select2/js/select2.full.min.js',
        'select2/js/i18n/ru.js',
        'js/fileinput.js',
        'js/article.js',
        'js/admin.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public function init()
    {
        parent::init();
        // resetting BootstrapAsset to not load own css files
        \Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
            'css' => [],
            'js' => []
        ];
        \Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapPluginAsset'] = [
            'css' => [],
            'js' => []
        ];
    }
}
