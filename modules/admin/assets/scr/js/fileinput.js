$(document).ready(function() {
    $('.image-fileinput-prew').each(function(index, element) {
        let previewImage = $(this).data('image');
        $(this).fileinput({
            theme: 'fas',
            language: 'ru',
            allowedFileExtensions: ['jpg', 'png', 'jpeg', 'svg'],
            initialPreviewAsData: true,
            initialPreview: [
                previewImage == '' ? null : '/web/' + previewImage,
            ],
            showUpload: false,
            showRemove: false,
            // maxFileSize: 2000,
        });

    });

    $('.video-preview').each(function(index, element) {
        let previewImage = $(this).data('image');
        $(this).fileinput({
            theme: 'fas',
            language: 'ru',
            allowedFileExtensions: ['mp4'],
            initialPreviewFileType: 'video',
            initialPreviewAsData: true,
            initialPreview: [
                previewImage == '' ? null : '/web/' + previewImage,
            ],
            initialPreviewConfig: [
                { type: "video", filetype: "video/mp4" }
            ],
            showUpload: false,
            showRemove: false,
        });

    });

    $('.image-fileinput').each(function(index, element) {
        $(this).fileinput({
            theme: 'fas',
            language: 'ru',
            allowedFileExtensions: ['jpg', 'png', 'jpeg', 'svg'],
            initialPreviewAsData: true,
            showUpload: false,
            showRemove: false,
            // maxFileSize: 2000,
        });

    });

    $('.fileinput').each(function(index, element) {
        $(this).fileinput({
            theme: 'fas',
            language: 'ru',
            allowedFileExtensions: ['jpg', 'png', 'jpeg', 'svg'],
            showUpload: false,
            showRemove: false,
            showPreview: false,
        });

    });
});