function getTimeSt() {
    let date = $('.form-get-time').val();
    $.ajax({
        type: "GET",
        url: "/admin/data/set-time",
        data: { 'date': date },
        success: function(response) {
            $('.getTimeData').val(response);
            // console.log(response);
        }
    });
}

function addVopros() {
    let value = [];
    let numb = 0;
    $('.lisVopros .input-group').each(function(index, element) {
        value[index] = $(this).find('input').val();
        numb = index + 1;
    });
    // console.log(value);
    let html = $('.lisVopros').html();
    let newHtml = '<div class="input-group mb-3"><div class="input-group-prepend"><span class="input-group-text" id="vopros' + numb + '"><span class="text-dark font-weight-bold">' + (numb + 1) + '</span></span></div><input type="text" id="vopros-' + numb + '-text" class="form-control" name="Vopros[' + numb + '][text]" aria-describedby="vopros' + numb + '" aria-invalid="false"><div class="help-block"></div></div>';
    $('.lisVopros').html(html + newHtml);

    $('.lisVopros .input-group').each(function(index, element) {
        value[index] = $(this).find('input').val(value[index]);
    });
}