function addArticleText(id) {
    $.ajax({
        type: "GET",
        url: "/admin/article-widget/add-text",
        data: { 'id': id },
        success: function(response) {
            $('.newContent').html(response);
            $('.wdgetAddBtn').remove();
            $('.ckedit').each(function(index, el) {
                var textName = $(this).attr('name');
                CKEDITOR.replace(textName, {
                    customConfig: '/web/lib/ckeditor/config-light.js'
                });
            });
            $('#addWidget').modal('hide');
        }
    });

}

function updateArticleText(id) {
    $.ajax({
        type: "GET",
        url: "/admin/article-widget/update-text",
        data: { 'id': id },
        success: function(response) {
            $('.bodyWidgetUpr' + id).html(response);
            $('.haderWidgetUpr' + id).html('');
            $('.wdgetAddBtn').remove();

            $('.ckedit').each(function(index, el) {
                var textName = $(this).attr('name');
                CKEDITOR.replace(textName, {
                    customConfig: '/web/lib/ckeditor/config-light.js'
                });
            });
        }
    });

}

function addArticleImage(id) {
    $.ajax({
        type: "GET",
        url: "/admin/article-widget/add-image",
        data: { 'id': id },
        success: function(response) {
            $('.newContent').html(response);
            $('.wdgetAddBtn').remove();
            $('#addWidget').modal('hide');
            $('.image-fileinput').each(function(index, element) {
                $(this).fileinput({
                    theme: 'fas',
                    language: 'ru',
                    allowedFileExtensions: ['jpg', 'png', 'jpeg', 'svg'],
                    initialPreviewAsData: true,
                    showUpload: false,
                    showRemove: false,
                    // maxFileSize: 2000,
                });

            });
        }
    });
}

function updateArticleImage(id) {
    $.ajax({
        type: "GET",
        url: "/admin/article-widget/update-image",
        data: { 'id': id },
        success: function(response) {
            $('.bodyWidgetUpr' + id).html(response);
            $('.haderWidgetUpr' + id).html('');
            $('.wdgetAddBtn').remove();

            $('.image-fileinput-prew').each(function(index, element) {
                let previewImage = $(this).data('image');
                $(this).fileinput({
                    theme: 'fas',
                    language: 'ru',
                    allowedFileExtensions: ['jpg', 'png', 'jpeg', 'svg'],
                    initialPreviewAsData: true,
                    initialPreview: [
                        previewImage == '' ? null : '/web/' + previewImage,
                    ],
                    showUpload: false,
                    showRemove: false,
                    // maxFileSize: 2000,
                });

            });
        }
    });
}

function addArticleDoc(id) {
    $.ajax({
        type: "GET",
        url: "/admin/article-widget/add-doc",
        data: { 'id': id },
        success: function(response) {
            $('.newContent').html(response);
            $('.wdgetAddBtn').remove();
            $('#addWidget').modal('hide');
        }
    });

}

function updateArticleDoc(id) {
    $.ajax({
        type: "GET",
        url: "/admin/article-widget/update-doc",
        data: { 'id': id },
        success: function(response) {
            $('.bodyWidgetUpr' + id).html(response);
            $('.haderWidgetUpr' + id).html('');
            $('.wdgetAddBtn').remove();
        }
    });

}


function removeWidget(url) {
    document.location.href = url;
}