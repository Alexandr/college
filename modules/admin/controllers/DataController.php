<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DataController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionSetTime($date)
    {
        return Yii::$app->formatter->asTimestamp($date, 'php:d.m.Y');
    }
}
