<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Gallery;
use app\modules\admin\models\NewGalery;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $models = Gallery::find()->indexBy('id')->orderBy(['weight' => SORT_ASC])->all();
        $newGalery = new NewGalery();
        $weight = count($models);

        if (Model::loadMultiple($models, Yii::$app->request->post())) {
            foreach ($models as $model) {
                $model->save(false);
            }
            return $this->redirect(['index']);
        }
        
        if ($newGalery->load(Yii::$app->request->post())) {
            $newGalery->imageFiles = UploadedFile::getInstances($newGalery, 'imageFiles');
            if ($newGalery->imageFiles) {
                $weight++;
                foreach ($newGalery->imageFiles as $key => $valueImage) {
                    $newModel = new Gallery();
                    $newModel->weight = $weight;
                    $newModel->imageFile = $valueImage;
                    if ($newModel->save()) {
                        $newModel->upload();
                    }
                }
            }
            return $this->redirect(['index']);
        }


        return $this->render('index', [
            'models' => $models,
            'newGalery' => $newGalery,
        ]);
    }

    
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->removeImages();
        $model->delete();
        $models = Gallery::find()->orderBy(['weight' => SORT_ASC])->all();
        if ($models != null) {
            foreach ($models as $key => $model) {
                $model->weight = $key;
                $model->save();
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
