<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Test;
use app\models\Vopros;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * TestController implements the CRUD actions for Test model.
 */
class TestController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Test models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Test::find()->orderBy(['weight'=>SORT_ASC]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Test model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Test model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Test();
        $vopros = [new Vopros()];

        if ($model->load(Yii::$app->request->post())) {
            $voprosPost = Yii::$app->request->post('Vopros', []);
            if (!empty($voprosPost)) {
                $newVopros = [];
                foreach ($voprosPost as $value) {
                    $newVopros[] = $value['text'];
                }
                $model->vopros = Json::encode($newVopros);
            }
            
            if ($model->save()) {
                return $this->redirect(['index']);
            }
            
        }

        return $this->render('create', [
            'model' => $model,
            'vopros' => $vopros
        ]);
    }

    /**
     * Updates an existing Test model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->vopros != null) {
            $vopros = Json::decode($model->vopros);
            foreach ($vopros as $key => $value) {
                $vopros[$key] = new Vopros();
                $vopros[$key]->text = $value;
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $voprosPost = Yii::$app->request->post('Vopros', []);
            if (!empty($voprosPost)) {
                $newVopros = [];
                foreach ($voprosPost as $value) {
                    $newVopros[] = $value['text'];
                }
                $model->vopros = Json::encode($newVopros);
            }
            
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'vopros' => $vopros
        ]);
    }

    /**
     * Deletes an existing Test model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Test model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Test the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Test::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAddVopros($id)
    {
        $vopros = [new Vopros()];

        return $this->render('add-vopros', [
            'id' => $id,
            'vopros' => $vopros
        ]);
    }
}
