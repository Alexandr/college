<?php

namespace app\modules\admin\controllers;

use app\models\ArticleDoc;
use app\models\ArticleImage;
use app\models\ArticleText;
use app\models\ArticleWidget;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class ArticleWidgetController extends Controller
{

    public function actionAddText($id)
    {
        $this->layout = false;
        $model = new ArticleText();
        $model->newModel($id);
        $url = Url::to(['/admin/article/update', 'id' => $id]);

        if ($model->load(Yii::$app->request->post())) {
            $model->saveModel();
            return $this->redirect($url);
        }

        return $this->render('add-text', [
            'model' => $model,
            'id' => $id,
            'url' => $url
        ]);
    }
    public function actionUpdateText($id)
    {
        $widgetId = ArticleWidget::findOne($id)->articleId;
        $this->layout = false;
        $model = new ArticleText();
        $model->openModel($id);
        $url = Url::to(['/admin/article/update', 'id' => $widgetId]);

        if ($model->load(Yii::$app->request->post())) {
            $model->saveModel();
            return $this->redirect($url);
        }

        return $this->render('add-text', [
            'model' => $model,
            'id' => $widgetId,
            'url' => $url
        ]);
    }

    public function actionAddImage($id)
    {
        $this->layout = false;
        $model = new ArticleImage();
        $model->newModel($id);
        $url = Url::to(['/admin/article/update', 'id' => $id]);

        if ($model->load(Yii::$app->request->post())) {
            $model->saveModel();
            return $this->redirect($url);
        }

        return $this->render('add-image', [
            'model' => $model,
            'id' => $id,
            'url' => $url
        ]);
    }

    public function actionUpdateImage($id)
    {
        $articleWidget = ArticleWidget::findOne($id);
        $widgetId = $articleWidget->articleId;
        $preview = $articleWidget->getImage()->getPath();
        $this->layout = false;
        $model = new ArticleImage();
        $model->openModel($id);
        $url = Url::to(['/admin/article/update', 'id' => $widgetId]);

        if ($model->load(Yii::$app->request->post())) {
            $model->saveModel();
            return $this->redirect($url);
        }

        return $this->render('update-image', [
            'model' => $model,
            'id' => $widgetId,
            'url' => $url,
            'preview' => $preview,
        ]);
    }


    public function actionAddDoc($id)
    {
        $this->layout = false;
        $model = new ArticleDoc();
        $model->newModel($id);
        $url = Url::to(['/admin/article/update', 'id' => $id]);

        if ($model->load(Yii::$app->request->post())) {
            $model->saveModel();
            return $this->redirect($url);
        }

        return $this->render('add-doc', [
            'model' => $model,
            'id' => $id,
            'url' => $url
        ]);
    }

    public function actionUpdateDoc($id)
    {
        $widgetId = ArticleWidget::findOne($id)->articleId;
        $this->layout = false;
        $model = new ArticleDoc();
        $model->openModel($id);
        $url = Url::to(['/admin/article/update', 'id' => $widgetId]);

        if ($model->load(Yii::$app->request->post())) {
            $model->saveModel();
            return $this->redirect($url);
        }

        return $this->render('add-doc', [
            'model' => $model,
            'id' => $widgetId,
            'url' => $url
        ]);
    }
}
