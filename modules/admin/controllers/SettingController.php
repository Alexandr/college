<?php

namespace app\modules\admin\controllers;

use app\models\FrontText;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

class SettingController extends Controller
{
    public function actionFrontText()
    {
        $model = new FrontText();
        $model->openModel();

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->image) {
                $model->upload();
            }
            $model->saveModel();
            return $this->redirect(['/admin/setting/front-text']);
        }
        return $this->render('front-text', [
            'model' => $model,
        ]);
    }
}
