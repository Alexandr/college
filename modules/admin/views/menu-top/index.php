<?php

use alex290\treemanager\TreeManager;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Верхнее меню';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-top-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?php if ($arrModel->count() > 0) : ?>
        <?= TreeManager::widget([
            'modelTree' => $arrModel,
        ]) ?>
    <?php endif ?>
</div>
