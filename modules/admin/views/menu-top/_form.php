<?php

use alex290\treeselect\TreeSelect;
use app\models\MenuTop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$map = MenuTop::find()->indexBy('id')->orderBy('weight')->asArray()->all();
$treeSelect = new TreeSelect();
?>

<div class="menu-top-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::merge(['0' => 'Основной'], $treeSelect->getTree($map))) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight')->hiddenInput()->label(false)?>

    <?= $form->field($model, 'link_attribute')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
