<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$numb = 0;

?>

<div class="test-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-8 pt-5 pb-5">
            <div class="lisVopros float-left w-100">
                <?php if ($vopros != null) : ?>
                    <?php foreach ($vopros as $index => $valueVopros) : ?>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="vopros<?= $index ?>"><span class="text-dark font-weight-bold"><?= $index + 1 ?></span></span>
                            </div>
                            <?= $form->field($valueVopros, "[$index]text", ['options' => ['tag' => false]])->textInput(['aria-describedby' => "vopros" . $index])->label(false) ?>
                        </div>
                        <?php $numb = $index + 1; ?>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
            <button type="button" class="btn btn-outline-dark" onclick="addVopros(<?= $numb ?>)"><i class="fas fa-plus"></i></button>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'name')->textInput() ?>

            <?= $form->field($model, 'vopros')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'otvet')->textInput(['type' => 'number']) ?>

            <?= $form->field($model, 'weight')->hiddenInput()->label(false) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>