<?php

?>
<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text" id="vopros<?= $id ?>"><?= $id +1  ?></span>
    </div>
    <input type="text" id="vopros-<?= $id ?>-text" class="form-control" name="Vopros[<?= $id ?>][text]" aria-describedby="vopros<?= $id ?>" aria-invalid="false">
    <div class="help-block"></div>
</div>