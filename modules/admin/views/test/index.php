<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тест';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute'=> 'vopros',
                'value' => function ($data) {
                    $html = '';
                    if ($data->vopros != null) {
                        $vopros = Json::decode($data->vopros);
                        foreach ($vopros as $key => $value) {
                            $html = $html."<p><b>".($key +1).": </b>".$value."</p>";
                        }
                    }
                    return $html;
                },
                'format' => 'raw',
            ],
            'otvet',
            [
                'value' => function ($data) {
                    $html =  Html::a('<i class="fas fa-edit"></i>', ['/admin/test/update', 'id' => $data->id], ['class' => 'btn btn-outline-success']);
                    $html = $html . Html::a('<i class="fas fa-trash-alt"></i>', ['/admin/test/delete', 'id' => $data->id], ['class' => 'btn btn-outline-danger', 'data-confirm' => "Вы уверены, что хотите удалить этот элемент?", 'data-method'=>'post']);
                    return $html;
                },
                'format' => 'raw',
            ],
        ],
    ]); ?>


</div>
