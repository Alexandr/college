<?php

use alex290\treeselect\TreeSelect;
use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$map = Category::find()->indexBy('id')->orderBy('weight')->asArray()->all();
$treeSelect = new TreeSelect();

$preview = null;
$imageClass = 'image-fileinput';
if (!$model->isNewRecord) {
    $preview = $model->getImage()->getPath();
    $imageClass = 'image-fileinput-prew';
}
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-8">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::merge(['0' => 'Основной'], $treeSelect->getTree($map))) ?>

            <?= $form->field($model, 'weight')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'alias')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-4">
            <div class="card border border-info">
                <div class="card-body">
                    <?= $form->field($model, 'imageFile')->fileInput(['class' => $imageClass, 'data-image' => $preview]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>