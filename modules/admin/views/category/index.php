<?php

use alex290\treemanager\TreeManager;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?php if ($arrModel->count() > 0) : ?>
        <?= TreeManager::widget([
            'modelTree' => $arrModel,
        ]) ?>
    <?php endif ?>


</div>