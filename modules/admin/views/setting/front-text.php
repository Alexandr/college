<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJsFile(
    '@web/web/lib/ckeditor/ckeditor.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$js = <<<JS
    $('.text-light').each(function(index, el) {
        var textName = $(this).attr('name');
        CKEDITOR.replace(textName, {
            customConfig: '/web/lib/ckeditor/config.js'
        });
    });
JS;

$this->registerJs($js);


$this->title = 'Текст на главной';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <?= $form->field($model, 'name')->textInput() ?>
                <?= $form->field($model, 'subName')->textInput() ?>
            </div>
            <div class="col-8">
                <?= $form->field($model, 'text')->textarea(['rows' => 10, 'class' => 'form-control text-light']) ?>
            </div>
            <div class="col-4">
                <?= $form->field($model, 'image')->fileInput(['class' => 'image-fileinput-prew', 'data-image' => $model->imageFile->getPath()]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>