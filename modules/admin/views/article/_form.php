<?php

use alex290\treeselect\TreeSelect;
use app\models\Category;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$map = Category::find()->indexBy('id')->orderBy('weight')->asArray()->all();
$treeSelect = new TreeSelect();

$preview = null;
$imageClass = 'image-fileinput';
if (!$model->isNewRecord) {
    $preview = $model->getImage()->getPath();
    $imageClass = 'image-fileinput-prew';
}



if (!$model->isNewRecord) {
    $classCollaps = '';
    $btnCollaps = '<i class="fas fa-angle-double-down"></i></button>';
} else {
    $classCollaps = 'show';
    $btnCollaps = '<i class="fas fa-angle-double-up"></i></button>';
}
?>

<div class="float-left w-100">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card border border-info">
        <div class="card-header d-flex justify-content-end">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><?= $btnCollaps ?></button>
        </div>
        <div class="collapse <?= $classCollaps ?>" id="collapseExample">
            <div class="card-body">
                <div class="row">
                    <div class="col-4">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'category_id')->dropDownList($treeSelect->getTree($map), ['prompt' => 'Выбирете категорию']) ?>

                        <?= $form->field($model, 'body')->textarea(['rows' => 6, 'class' => 'form-control']) ?>

                        <?= $form->field($model, 'frontpage')->hiddenInput()->label(false) ?>

                        <?= $form->field($model, 'alias')->hiddenInput()->label(false) ?>



                        <?= $form->field($model, 'count')->hiddenInput()->label(false) ?>
                        <div class="form-group">
                            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card border border-info">
                            <div class="card-body">
                                <?= $form->field($model, 'imageFile')->fileInput(['class' => $imageClass, 'data-image' => $preview]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card border border-info">
                            <div class="card-body">
                                <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

                                <?= $form->field($model, 'date_create')->hiddenInput(['class' => 'getTimeData']) ?>
                                <?= Html::textInput('data', Yii::$app->formatter->asDate($model->date_create, 'yyy-MM-dd'), ['type' => 'date', 'class' => 'form-control form-get-time', 'onchange' => "getTimeSt();"]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php if (!$model->isNewRecord) : ?>
    <?php if ($model->articleWidgets != null) : ?>
        <?php foreach ($model->articleWidgets as $key => $widget) : ?>
            <div class="card" data-id=<?= $widget->id ?>>
                <?php if ($widget->type == 1) : ?>
                    <?= $this->render('widget/text', [
                        'widget' => $widget,
                    ]) ?>
                <?php elseif ($widget->type == 2) : ?>
                    <?= $this->render('widget/image', [
                        'widget' => $widget,
                    ]) ?>
                <?php elseif ($widget->type == 3) : ?>
                    <?= $this->render('widget/doc', [
                        'widget' => $widget,
                    ]) ?>
                <?php endif ?>
            </div>
        <?php endforeach ?>
    <?php endif ?>
    <div class="float-left w-100 newContent"></div>
    <div class="float-left w-100 d-flex justify-content-center mt-5 wdgetAddBtn">
        <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#addWidget"><i class="fas fa-plus"></i></button>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addWidget" tabindex="-1" role="dialog" aria-labelledby="addWidgetLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addWidgetLabel">Добавить содержимое</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body d-flex flex-wrap">
                    <button type="button" class="btn btn-outline-dark btn-lg mr-3 ml-3 mb-4" onclick="addArticleText(<?= $model->id ?>)">Текст</button>
                    <button type="button" class="btn btn-outline-dark btn-lg mr-3 ml-3 mb-4" onclick="addArticleImage(<?= $model->id ?>)">Изображение</button>
                    <button type="button" class="btn btn-outline-dark btn-lg mr-3 ml-3 mb-4" onclick="addArticleDoc(<?= $model->id ?>)">Файл(Документ)</button>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>