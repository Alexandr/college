<?php

use alex290\treeselect\TreeSelect;
use app\models\Category;
use yii\helpers\Html;
use yii\grid\GridView;

$map = Category::find()->indexBy('id')->orderBy('weight')->asArray()->all();
$treeSelect = new TreeSelect();

$this->title = 'Статьи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'category_id',
                'value' => function ($data) {
                    return $data->category->name;
                },
                'filter' => $treeSelect->getTree($map),
            ],
            'date_create:date',
            // 'body:ntext',
            //'frontpage',
            //'alias',
            //'meta_description',
            //'count',

            [
                'value' => function ($data) {
                    $html =  Html::a('<i class="fas fa-edit"></i>', ['/admin/article/update', 'id' => $data->id], ['class' => 'btn btn-outline-success']);
                    $html = $html . Html::a('<i class="fas fa-eye"></i>', ['/article/view', 'id' => $data->id], ['class' => 'btn btn-outline-secondary']);
                    $html = $html . Html::a('<i class="fas fa-trash-alt"></i>', ['/admin/article/delete', 'id' => $data->id], ['class' => 'btn btn-outline-danger', 'data-confirm' => "Вы уверены, что хотите удалить этот элемент?", 'data-method'=>'post']);
                    return $html;
                },
                'format' => 'raw',
            ],
        ],
    ]); ?>


</div>
