<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;



$this->title = 'Галерея';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="d-flex justify-content-between">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="card">
    <div class="card-body pt-5 pb-5">

        <?php if ($models != null) : ?>
            <?php $formGall = ActiveForm::begin(['options' => ['class' => 'w-100']]); ?>
            <div class="row">
                <div class="col-12">
                    <?= Html::submitButton('Обновить', ['class' => 'btn btn-success']) ?>
                </div>
                <?php foreach ($models as $key => $model) : ?>
                    <div class="col-3">
                        <div class="card border border-info mt-3">
                            <div class="card-header">
                                <?= $formGall->field($model, "[$key]weight")->hiddenInput()->label(false) ?>
                                <?= $formGall->field($model, "[$key]name")->textInput() ?>
                            </div>
                            <img src="/web/<?= str_replace("\\", "/", $model->getImage()->getPath('300x300')) ?>" alt="" class="img-fluid">
                            <div class="card-footer d-flex justify-content-between">
                                <?= Html::a('<i class="fas fa-trash-alt"></i>', ['/admin/gallery/delete', 'id' => $model->id], ['class' => 'btn btn-outline-danger', 'data-confirm' => "Вы уверены, что хотите удалить этот элемент?", 'data-method' => 'post']) ?>
                            </div>
                        </div>
                    </div>

                <?php endforeach ?>
            </div>
            <?php ActiveForm::end(); ?>
        <?php endif ?>

    </div>
    <div class="card-body border border-info">
        <div class="gallery-form">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($newGalery, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' => 'image-fileinput']) ?>

            <div class="form-group">
                <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>