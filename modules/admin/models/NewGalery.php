<?php

namespace app\modules\admin\models;

use yii\base\Model;

class NewGalery extends Model
{

    public $imageFiles;


    public function rules()
    {
        return [
            [['title'], 'string'],
            [['imageFiles'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'imageFiles' => 'Новые изображения',
        ];
    }
}
